use <./illumination.scad>;
include <./microscope_parameters.scad>;


  //NOTE: set torch diameter using value of 'torch' in module
  //tweaktorch_z variable which gives vertical offset of clip to fit different torches: torch_z=0 is easier to print; -20 looks cooler!

//Two versions of holder are useful- can be rendered using two options below:

//OPTION 1
mirror([0,0,1]) torch_illumination(torch=20, torch_z=-30); 
// IF taller version needed, comment out tiled version  below and use module alone mirrored above, and print vertically

//If shorter version needed, use 15 degree cutoff version below and print tilted so cutoff face flat on bed.
//OPTION 2
/*
difference(){
 translate([0,0,12])   rotate([-40,0,0]) torch_illumination(torch=20, torch_z=-30);
    mirror([0,0,1]) cylinder(r=999,h=999,$fn=4);}  
    */ 
